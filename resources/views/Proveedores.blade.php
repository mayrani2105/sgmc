<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet"
  href="{{ asset('assets/css/bootstrap.min.css') }}" />  <link rel="stylesheet" href="{{ asset('assets/css/simple-sidebar.css') }}" />
  <title>SMGC | Jefe de Operaciones </title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>

  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>

  <!-- Custom styles for this template -->
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

</head>
  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>
<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->

    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #489bee;">

      <div class="sidebar-heading bg-white"><a href=""><img  href="JefeOp" src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a></div>
      <div class="list-group list-group-flush" id="sidebar-list">


<div class="dropdown">
        <a href="" class=" list-group-item list-group-item-action bg-transparent" id="list-element" onclick="myFunctiond"><i class="fas fa-dolly"></i> Inventarios</a>
        <ul id="drop" class="dropdown-content">
          <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user fa-1x"></i> Ver inventario</a>
              <a class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden ><i class="fas fa-user fa-1x"></i> Modificar inventario</a>

</ul></div>
<script>
function myFunctiond(){
  document.getElementById("list-element").classList.toggle("show");
}
</script>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user-shield"></i> Roles</a>
        <ul>
        <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fas fa-user-shield"></i> Modificar roles</a></ul>


		<a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-truck"></i> Pedidos</a>
    <ul>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i> Editar Pedido</a>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i> Ver Pedidos</a>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i> Generar Cancelacion</a></ul>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-box"></i> Productos</a>
          <ul>

              <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="far fa-scanner"></i> Agregar Producto</a>
            <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i>Modificar Producto</a>
                <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i>Eliminar Producto</a></ul>
                <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
                <ul>
                    <a class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user fa-1x"></i>Reporte inventarios</a>
                  </ul>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-file-alt"></i> Marbete</a>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1"></i> Perfil</a>
    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-sign-out-alt fa-1x"></i> Cerrar Sesión</a>
      </div>
    </div>

    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

<h4 class="text-transparent">  SGMC</h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href=" "><i class="fas fa-user-tie fa-1x"></i> Jefe de Operaciones</a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <nav class="navbar navbar-light bg-light">
    <form class="form-inline">
      <input class="form-control" type="search" placeholder="Buscar" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    </form>

  </nav>

  <div>
  <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Nombre </th>
      <th scope="col">Apellidos </th>
      <th scope="col">Localidad </th>
      <th scope="col">Teléfono </th>
      <th scope="col">Domicilio </th>
      <th scope="col">Número de Proveedor </th>
      <th scope = "col" style="padding: 0;">  </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Juan Luis</th>
      <td>López Sánchez</td>
      <td>Morelia, Michoacán </td>
      <td>4431322425</td>
      <td>Calle de los Bosques #339</td>
      <td>100 </td>
      <td style="padding: 0;">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-1 my-sm-0" value="Modificar Datos">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-danger my-1 my-sm-0" value="Dar de Baja">
       </td>
    </tr>
    <tr>
      <th>Francisco Antonio </th>
      <td>Olivares Abad </td>
      <td>Morelia, Michoacán </td>
      <td>4431337876 </td>
      <td>Calle Juan Escutia #23 </td>
      <td>258 </td>
      <td style="padding: 0;">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-1 my-sm-0" value="Modificar Datos">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-danger my-1 my-sm-0" value="Dar de Baja">
       </td>
    </tr>
    <tr>
      <th>Alberto Carlos </th>
      <td>Luquin Alcalá </td>
      <td>Guadalajara, Jalisco </td>
      <td>3345678909</td>
      <td>Insurgentes Norte #98</td>
      <td>030</td>
      <td style="padding: 0;">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-1 my-sm-0" value="Modificar Datos">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-danger my-1 my-sm-0" value="Dar de Baja">
       </td>
    </tr>
    <tr>
      <th>Gabriela</th>
      <td>Lúa Vargas </td>
      <td>Morelia, Michoacán </td>
      <td>4439098767 </td>
      <td>Chapultepec Sur #780 </td>
      <td>456</td>
      <td style="padding: 0;">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-1 my-sm-0" value="Modificar Datos">
            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-danger my-1 my-sm-0" value="Dar de Baja">
       </td>
    </tr>

  </tbody>
</table>
</div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>

</html>
