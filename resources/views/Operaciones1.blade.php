<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SMGC | Panel de administración</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />  <link rel="stylesheet" href="{{ asset('assets/css/simple-sidebar.css') }}" />
  <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>

  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>

  <!-- Custom styles for this template -->
  <link href="/css/simple-sidebar.css" rel="stylesheet">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

</head>
  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>
<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->

    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #489bee;" >

      <div class="sidebar-heading bg-white"><a href=""><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a></div>
      <div class="list-group list-group-flush" id="sidebar-list">


<div class="dropdown">
        <a href="" class=" list-group-item list-group-item-action bg-transparent" id="list-element" onclick="myFunctiond"><i class="fas fa-dolly"></i> Inventarios</a>
        <ul id="drop" class="dropdown-content">
          <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user fa-1x"></i> Ver inventario</a>
              <a class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden ><i class="fas fa-user fa-1x"></i> Modificar inventario</a>

</ul></div>
<script>
function myFunctiond(){
  document.getElementById("list-element").classList.toggle("show");
}
</script>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user-shield"></i> Roles</a>
        <ul>
        <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fas fa-user-shield"></i> Modificar roles</a></ul>


		<a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-truck"></i> Pedidos</a>
    <ul>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i> Editar Pedido</a>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i> Ver Pedidos</a>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i> Generar Cancelacion</a></ul>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-box"></i> Productos</a>
          <ul>

              <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="far fa-scanner"></i> Agregar Producto</a>
            <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i>Modificar Producto</a>
                <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i>Eliminar Producto</a></ul>
                <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
                <ul>
                    <a class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user fa-1x"></i>Reporte inventarios</a>
                  </ul>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-file-alt"></i> Marbete</a><br/>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user  fa-1"></i> Perfil</a>

    <br/>  <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-sign-out-alt fa-1x"></i> Cerrar Sesión</a>
      </div>
    </div>

    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

<h4 class="text-primary">  SGMC</h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href=" "><i class="fas fa-user-tie fa-1x"></i> Jefe de Operaciones</a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <nav class="navbar navbar-light bg-light">
    <form class="form-inline">
      <input class="form-control" type="search" placeholder="Buscar" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    </form>

  </nav>

  <div class="container" >
    <h2>Creación de Productos</h2>
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-login">
          <div class="panel-heading">

            <hr>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">

                <form id="register-form" action="http://phpoll.com/register/process" method="post" role="form">
                  <div class="form-group">
                      <input type="text" name="Clave" id="Clave" tabindex="1" class="form-control" placeholder="Clave" value="" width="20%"><br/>
                        <input type="text" name="Clavesat" id="Clavesat" tabindex="1" class="form-control" placeholder="Clave Sat" value="" width="20%"><br/>
                        <input type="textarea" name="descripcion" id="descripcion"  class="form-control" placeholder="Descripción" maxlength="300px" value=""><br/>
                    <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Nombre del Producto" value="">
                  </div>
                  <div class="form-group">
                    <select type="" name="email" id="linea" tabindex="" class="form-control" placeholder="Linea" value="">
                      <option selected>Linea</option>
                      <option value ="1">Cuaderno</option></select><br/>
                      <select type="" name="email" id="linea" tabindex="" class="form-control" placeholder="Linea" value="">
                        <option selected>SubLinea</option>
                        <option value ="1">Cuaderno Italiano</option></select>
                  </div>

                  <div class="form-group">
                    <select type="text" name="Unidad-base" id="Unidad-base"  class="form-control" placeholder="Unidad/Base" value="Proveedor">
                       <option selected>Unidad/Base</option>
                      <option value ="1">Pieza</option>
                        <option value ="2">Base</option></select><br/>
                    <select type="text" name="Marca" id="Marca"  class="form-control" placeholder="Marca" value="Proveedor">
                       <option selected>Marca</option>
                      <option value ="1">FaberCaster</option></select><br/>
                    <select type="text" name="Proveedor" id="Proveedor"  class="form-control" placeholder="Linea" value="Proveedor">
                        <option selected>Proveedor</option>
                      <option value ="1">FaberCaster</option></select>
                  </div>
                  <div class="form-group">
                    <input type="number" name="costo" id="costo" tabindex="2" class="form-control" placeholder="Costo">
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6 col-sm-offset-3">
                        <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-2 my-sm-0" value="Crear Producto">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <table class="table table-sm" hidden>
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Codigo</th>
      <th scope="col">Estado</th>
      <th scope="col">Proveedor</th>
      <th scope="col">Costo</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Codigo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2">Larry the Bird</td>
      <td>@twitter</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="3">Larry the Bird</td>
      <td>@twitter</td>
    </tr>

  </tbody>
</table>
</hr>
<br/><br/>
<table class="table table-sm" hidden>
<thead>
  <tr>
    <th scope="col">Nombre</th>
    <th scope="col">Codigo</th>
    <th scope="col">Estado</th>
    <th scope="col">Proveedor</th>
    <th scope="col">Costo</th>
    <th scope="col">Cantidad</th>
    <th scope="col">Codigo</th>
  </tr>
</thead>
<tbody>
  <tr>
    <th scope="row">Lapices</th>
    <td>034343</td>
      <td BGCOLOR="#00FF00"></td>
    <td>@mdo</td>
  </tr>
  <tr>
    <th scope="row">2</th>
    <td>Jacob</td>
    <td BGCOLOR="#00FF00"></td>
    <td>Marinela</td>
    <td>50.0</td>
  </tr>
  <tr>
    <th scope="row">3</th>
    <td colspan="2">Larry the Bird</td>
    <td>@twitter</td>
  </tr>
  <tr>
    <th scope="row">3</th>
    <td colspan="2">Larry the Bird</td>
    <td>@twitter</td>
  </tr>

</tbody>
</table>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>

</html>
