<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SMGC | Jefe de almacen</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet"
  href="{{ asset('assets/css/bootstrap.min.css') }}" />  <link rel="stylesheet" href="{{ asset('assets/css/simple-sidebar.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
  <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>

  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>

  <!-- Custom styles for this template -->
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

</head>
  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>
<body>
  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->

    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #489bee;" >

   <div class="sidebar-heading bg-white"><a href=""><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a></div>
      <div class="list-group list-group-flush" id="sidebar-list">


        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-cash-register"></i> Ventas</a>
		<a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-shopping-cart"></i> Compras</a>
		<a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-book-open"></i> Catalogos</a>
        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-chart-bar"></i> Reportes</a>
		<a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
      </div>
    </div>

    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
		<h4 class="text-primary">  SGMC</h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#"><i class="fas fa-user-check fa-1x"></i> Jefe de Almacen</a>
            </li>
          </ul>
        </div>
      </nav>

    <div class="container-fluid">
    	<nav class="navbar navbar-light bg-light">
    		<form class="form-inline">
      			<input class="form-control" type="search" placeholder="Buscar" aria-label="Search">
      			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    		</form>
  		</nav>

  	<div class="container">
		<div class="form-row col-md-12">
			<div class="col-md-10">
				<h2>Ventas</h2>
			</div>
			<div class="form-group col-md-2">
				<label for="fecha">Ver ventas</label>
				<select class="form-control col-md-12">
					<option value="0" selected>Seleccione</option>
					<option value="1">Del día</option>
					<option value="2">Mensuales</option>
					<option value="1">Anuales</option>
				</select>
			</div>

		</div>

		<div class="card">
		  <div class="card-header">
			  <i class="fas fa-cash-register"></i> Ultimas ventas
		  </div>
  		</div>
		<table class="table">
		  <thead>
			<tr>
			  <th scope="col">No. Venta</th>
			  <th scope="col">Factura/Remision</th>
			  <th scope="col">Producto</th>
			  <th scope="col">Cantidad</th>
			  <th scope="col">Precio unitario</th>
			  <th scope="col">Total</th>
			</tr>
		  </thead>
		  <tbody>
			<tr>
				<td>1001</td>
				<td>2150</td>
				<td>un producto</td>
				<td>100</td>
				<td>$5.00</td>
				<td>$5,000.00</td>
			</tr>
		  </tbody>
		</table>
		<br><br>
		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
		<script>
			Highcharts.chart('container', {
			  chart: {
				zoomType: 'xy'
			  },
			  title: {
				text: 'Ventas mensuales dentro de CONTI'
			  },
			  subtitle: {
				text: 'Ventas registradas mensualmente en el sistema'
			  },
			  xAxis: [{
				categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
				  'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				crosshair: true
			  }],
			  yAxis: [{ // Primary yAxis
				labels: {
				  format: '{value}%',
				  style: {
					color: Highcharts.getOptions().colors[1]
				  }
				},
				title: {
				  text: 'Aumento',
				  style: {
					color: Highcharts.getOptions().colors[1]
				  }
				}
			  }, { // Secondary yAxis
				title: {
				  text: 'Venta mensual',
				  style: {
					color: Highcharts.getOptions().colors[0]
				  }
				},
				labels: {
				  format: '${value}K MXN',
				  style: {
					color: Highcharts.getOptions().colors[0]
				  }
				},
				opposite: true
			  }],
			  tooltip: {
				shared: true
			  },
			  legend: {
				layout: 'vertical',
				align: 'left',
				x: 120,
				verticalAlign: 'top',
				y: 100,
				floating: true,
				backgroundColor:
				  Highcharts.defaultOptions.legend.backgroundColor || // theme
				  'rgba(255,255,255,0.25)'
			  },
			  series: [{
				name: 'Venta mensual',
				type: 'column',
				yAxis: 1,
				data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
				tooltip: {
				  valueSuffix: ' $'
				}

			  }, {
				name: 'Aumento',
				type: 'spline',
				data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
				tooltip: {
				  valueSuffix: '%'
				}
			  }]
			});
		</script>

    </div>
 </div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>

</html>
