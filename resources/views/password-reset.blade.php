<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="jpt5tsqDC0nvX3yp5pCNx7PgjuJyIC4RXVPw365B">


    <title>SGMC | CONTI</title>

  <link href="admin.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/admin.css') }}" />


</head>

<body class="app header-fixed sidebar-fixed sidebar-lg-show">

        <div class="container" id="app">
        <div class="row align-items-center justify-content-center auth">
            <div class="col-md-6 col-lg-5">
                <div class="card">
                    <div class="card-block">
                        <auth-form
                            :action="'https://demo.getcraftable.com/admin/login'"
                            :data="{}"
                            inline-template>
                            <form class="form-horizontal" role="form" method="POST" action="https://demo.getcraftable.com/admin/login" novalidate>
                                <input type="hidden" name="_token" value="jpt5tsqDC0nvX3yp5pCNx7PgjuJyIC4RXVPw365B">
                                <div class="auth-header" >
                                    <img class="mb-4"src="{{ asset('assets/Images/logo.jpg') }}" alt="" width="250" height="88.5">
                                    <h1 class="auth-title">Recuperación</h1>
                                </div>
                                <div class="auth-body">
                                    <div class="form-group" :class="{'has-danger': errors.has('email'), 'has-success': this.fields.email && this.fields.email.valid }">
                                        <label for="email">Correo Electronico</label>
                                        <div class="input-group input-group--custom">
                                            <div class="input-group-addon">
                                                <i class="input-icon input-icon--mail"></i>
                                            </div>
                                            <input type="text" v-model="form.email" v-validate="'required|email'" class="form-control" :class="{'form-control-danger': errors.has('email'), 'form-control-success': this.fields.email && this.fields.email.valid}" id="email" name="email" placeholder="Correo Electronico">
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="remember" value="1">
                                        <button type="submit" class="btn btn-primary btn-block btn-spinner"><i class="fa"></i> Enviar enlace</button>
                                    </div>
                                    <div class="form-group text-center">
                                        <a href="index.php  ?>" class="auth-ghost-link">¿Iniciar sesión</a>
                                    </div>
                                </div>
                            </form>
                        </auth-form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="/js/admin.js"></script>        <script type="text/javascript">
        // fix chrome password autofill
        // https://github.com/vuejs/vue/issues/1331
        document.getElementById('password').dispatchEvent(new Event('input'));
    </script>
</body>

</html>
