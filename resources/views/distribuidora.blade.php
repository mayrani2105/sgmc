<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>SMGC | Distribuidora</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/checkout/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet"
    href="{{ asset('assets/css/bootstrap.min.css') }}" />  <link rel="stylesheet" href="{{ asset('assets/css/simple-sidebar.css') }}" />
    <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

    </style>
    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
</head>
<style>
    hello:hover {
        background-color: #fff;
        width: 100%;
        height: 100%;
    }

</style>

<body class="bg-light">
    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->

        <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #489bee;">

            <div class="sidebar-heading bg-white"><a href=""><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a></div>
            <div class="list-group list-group-flush" id="sidebar-list">


                <div class="dropdown">
                    <a href="" class=" list-group-item list-group-item-action bg-transparent" id="list-element" onclick="myFunctiond"><i class="fas fa-dolly"></i> Inventarios</a>
                    <ul id="drop" class="dropdown-content">
                        <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user fa-1x"></i> Ver inventario</a>
                        <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user fa-1x"></i> Modificar inventario</a>

                    </ul>
                </div>
                <script>
                    function myFunctiond() {
                        document.getElementById("list-element").classList.toggle("show");
                    }

                </script>


                <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-truck"></i> Pedidos</a>
                <ul>

                    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i> Editar Pedido</a>
                    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i> Ver Pedidos</a>
                    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i> Generar Cancelacion</a></ul>
                <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-box"></i> Productos</a>
                <ul>

                    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="far fa-scanner"></i> Agregar Productos</a>
                    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i>Modificar Productos</a>
                    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i>Eliminar Productos</a></ul>


                <br /> <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-sign-out-alt fa-1x"></i> Cerrar Sesión</a>
            </div>
        </div>

        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
                <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <h4 class="text-primary"> SGMC</h4>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href=" "><i class="fas fa-user-tie fa-1x"></i> Distribuidora</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="container">
                <nav class="navbar navbar-light bg-light">
                    <form class="form-inline">
                        <input class="form-control" type="search" placeholder="Buscar producto" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar producto</button>
                    </form>

                </nav>

                <div class="container">
                    <h2>Generación de pedido</h2>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="panel panel-login">
                                <div class="panel-heading">

                                    <hr>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="container">

                                                <div class="row">
                                                    <div class="col-md-4 order-md-2 mb-4">



                                                        <form class="card p-2">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="Folio">
                                                                <div class="input-group-append">
                                                                    <button type="submit" class="btn btn-secondary">Buscar folio</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="col-md-8 order-md-1">
                                                        <h4 class="mb-3">Datos de envio</h4>
                                                        <form class="needs-validation" novalidate>
                                                            <div class="row">
                                                                <div class="col-md-6 mb-3">
                                                                    <label for="firstName">Nombre encargado</label>
                                                                    <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                                                                    <div class="invalid-feedback">
                                                                        Nombre es requerido.
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 mb-3">
                                                                    <label for="lastName">Apellido Paterno</label>
                                                                    <input type="text" class="form-control" id="lastName" placeholder="" value="" required>
                                                                    <div class="invalid-feedback">
                                                                        Apellidos son requeridos.
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="mb-3">
                                                                <label for="email">Email <span class="text-muted">(Optional)</span></label>
                                                                <input type="email" class="form-control" id="email" placeholder="you@example.com">
                                                                <div class="invalid-feedback">
                                                                    Email no valido.
                                                                </div>
                                                            </div>

                                                            <label for="address2">Almacen de origen</label>
                                                            <div class="form-group">


                                                                <input type="text" name="Marca" id="Marca" class="form-control" placeholder="Marca" value="Almacen origen" readonly>
                                                                    <br />

                                                                <input type="text" name="calle" id="Calle" tabindex="1" class="form-control" placeholder="Calle origen" value="" width="20%" readonly><br />
                                                                <input type="text" name="colonia" id="Colonia" tabindex="1" class="form-control" placeholder="Colonia origen" value="" readonly width="20%"><br />
                                                                <input type="textarea" name="numero" id="Numero" class="form-control" placeholder="Numero origen" maxlength="300px" value="" readonly><br />


                                                            </div>
                                                            <label for="address2">Seleccione almacen de destino</label>
                                                            <div class="form-group">


                                                                <select type="text" name="Marca" id="Marca" class="form-control" placeholder="Marca" value="Direccion-env">
                                                                    <option selected>Almacen a</option>
                                                                    <option value="1">Almacen b</option>
                                                                </select><br />

                                                                <input type="text" name="calle" id="Calle" tabindex="1" class="form-control" placeholder="Calle destino" value="" readonly width="20%"><br />
                                                                <input type="text" name="colonia" id="Colonia" tabindex="1" class="form-control" placeholder="Colonia destino" readonly value="" width="20%"><br />
                                                                <input type="textarea" name="numero" id="Numero" class="form-control" placeholder="Numero destino" maxlength="300px" readonly value=""><br />

                                                                <label for="address2">Seleccione chofer</label>
                                                            <div class="form-group">


                                                                <select type="text" name="Chofer" id="chofer" class="form-control" placeholder="Chofer" value="Chofer-env">
                                                                    <option selected>Chofer 1</option>
                                                                    <option value="1">Chofer 2</option>
                                                                </select><br />

                                                            </div>


                                                            <div class="bd-example">
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">ID</th>
                                                                            <th scope="col">Nombre</th>
                                                                            <th scope="col">Fabricante</th>
                                                                            <th scope="col">Cantidad</th>

                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr class="table-active">
                                                                            <th scope="row">1</th>
                                                                            <td>Lapiz</td>
                                                                            <td>Lapicines</td>
                                                                            <td>100</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">2</th>
                                                                            <td>Paquete hojas</td>
                                                                            <td>Hojas</td>
                                                                            <td>40</td>
                                                                        </tr>


                                                                        <tr class="table-primary">
                                                                            <th scope="row">3</th>
                                                                            <td>Goma blanca</td>
                                                                            <td>Mundo de la goma</td>
                                                                            <td>200</td>
                                                                        </tr>
                                                                        <tr class="table-secondary">
                                                                            <th scope="row">4</th>
                                                                            <td>Laptop</td>
                                                                            <td>Dell</td>
                                                                            <td>90</td>
                                                                        </tr>
                                                                        <tr class="table-success">
                                                                            <th scope="row">5</th>
                                                                            <td>Libreta</td>
                                                                            <td>Norma</td>
                                                                            <td>80</td>
                                                                        </tr>
                                                                        <tr class="table-danger">
                                                                            <th scope="row">6</th>
                                                                            <td>Carpeta</td>
                                                                            <td>Carpetas inc</td>
                                                                            <td>800</td>
                                                                        </tr>
                                                                        <tr class="table-warning">
                                                                            <th scope="row">7</th>
                                                                            <td>Pluma</td>
                                                                            <td>BIC</td>
                                                                            <td>708</td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>



                                                            <button class="btn btn-primary btn-lg btn-block" type="submit">Proceder a enviar pedido</button>
                                                            </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <footer class="my-5 pt-5 text-muted text-center text-small">
                                                    <p class="mb-1">&copy; 2019 SGCM </p>
                                                </footer>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <br /><br />

            </div>
        </div>
        <!-- /#page-content-wrapper -->
        <!-- /#wrapper -->

        <!-- Bootstrap core JavaScript -->
        <script src="/jquery/jquery.min.js"></script>
        <script src="/js/bootstrap.bundle.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });

        </script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>



        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script>
            window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')

        </script>
        <script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
        <script src="form-validation.js"></script>
    </div>
</body>

</html>
