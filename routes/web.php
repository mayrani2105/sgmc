<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::any('/JefeOp', function () {
    return view('Operaciones');
});
Route::any('/JefeOp1', function () {
    return view('Operaciones1');
});
Route::any('/Checador', function () {
    return view('Checador');
});
Route::any('/Distribuidora', function () {
    return view('distribuidora');
});
Route::any('/password-reset', function () {
    return view('password-reset');
});
Route::any('/JefeA', function () {
    return view('JefeAlmacen');
});
Route::any('/ProdEliminar', function () {
    return view('OperacionesElim');
});
Route::any('/Crear', function () {
    return view('CrearUsuario');
});

Route::any('/ModificarProducto', function () {
    return view('OperacionesModi');
});

Route::any('/Proveedor', function () {
    return view('Proveedores');
});
Route::any('/Productos', function () {
    return view('CatalogoProductos');
});
